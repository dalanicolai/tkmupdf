#!/usr/bin/env python3

import sys
import os

import tkinter as tk

import fitz

import annotation

if len(sys.argv) == 2:
    fname = sys.argv[1]
else:
    fname ='/home/dalanicolai/git/tkpymupdf/test.pdf'

root = tk.Tk()
root.title('pyPdfEditor')

screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()

doc = fitz.open(fname)
page_count = len(doc)
dlist= []
page_sizes = []     # list of of pagesizes as mupdf Rect(x1, y1, x2, y2) objects
for i in range(page_count):
    pagedl = doc[i].getDisplayList()
    dlist.append(pagedl)
    page_sizes.append((pagedl.rect))

def get_most_frequent(orientation):
    '''Get most frequent page horizontal/vertical resolution

    :param orientation (int): 2 = horizontal, 3 = vertical
    :return: integer: most frequent horizontal/vertical page resolution
    '''
    frequency= {}
    for i in page_sizes:
        if i[orientation] in frequency.keys():
            frequency[i[orientation]] += 1
        else:
            frequency[i[orientation]]=0
    return max(frequency, key=frequency.get)

width = most_freq_page_width  = get_most_frequent(2)
height = most_freq_page_height = get_most_frequent(3)

zoom_factor = 1.0 #screen_height / height   # initial view fit page to screen vertically
print(zoom_factor)
pagewidth = width * zoom_factor
pageheight = height * zoom_factor

doc_height = sum([zoom_factor * ps[3] for ps in page_sizes]) # calculate size canvas to fit all pages

title = "PyMuPDF display of '%s', pages: %i" % (os.path.basename(fname), page_count)

def get_page(page, zoom_factor):
    global dlist
    pdlist = dlist[page]
    magn_mat = fitz.Matrix(zoom_factor, zoom_factor)
    pix = pdlist.getPixmap(matrix=magn_mat, alpha=False)
    imdata = pix.getImageData("ppm")
    return imdata

def load_images(pno, zoom_factor):
    '''Create list of 3 pages with previous, current(pno) and next page

    :param pno (int):
    :return: list: List containing 3 tk photoimages
    '''
    if pno == 0:
        plist = [pno, pno+1]
    elif pno == page_count-1:
        plist = [pno-1,pno]
    else:
        plist = [pno-1,pno,pno+1]

    imlist = [get_page(i, zoom_factor) for i in plist]

    tkimdict = {i:tk.PhotoImage(data=imlist[i]) for i in plist}
    return tkimdict

def create_cframe(parent, width, height):
    '''Create parent frame for canvas'''
    cframe = tk.Frame(parent, width=width, height=(height-40))
    return cframe

def create_canvas(width, height):
    # global zoom_factor
    w = tk.Canvas(cframe, bg='white', width=zoom_factor * width, height=height, highlightcolor='white')
    w.config(scrollregion=(0, 0, zoom_factor * width, zoom_factor * doc_height))
    w.focus_set()
    w.bind("<Key>", key)
    w.bind("<Button-4>", mouse_wheel)
    return w

def point_to_page():
    return int(round(pointer * page_count))


def render_page(page, zoom_factor):
    imdata = get_page(page, zoom_factor)
    tkimdict[page] = tk.PhotoImage(data=imdata)
    image_dict[page] = create_pageim(page)

def delete_all_pages(page):
    w.delete(image_dict[page])
    try: tkimdict.pop(page)
    except: pass
    if not page == 0:
        w.delete(image_dict[page - 1])
        try: tkimdict.pop(page - 1)
        except: pass
    if not page == page_count - 1:
        w.delete(image_dict[page + 1])
        try: tkimdict.pop(page + 1)
        except: pass

def render_all_pages(page):
    if not page in [0, page_count - 1]:
        pagelist = [page - 1, page, page + 1]
    elif page == 0:
        pagelist = [page, page + 1]
    elif page == page_count - 1:
        pagelist = [page - 1, page]
    for i in pagelist:
        print(i)
        print('dlist', len(dlist))
        render_page(i, zoom_factor)

def zoom_refresh(page):
    delete_all_pages(page)
    cframe.config(width=zoom_factor * width, height=(height - 40))
    w.config(width=zoom_factor * width, height=height)
    w.config(scrollregion=(0, 0, zoom_factor * width, zoom_factor * doc_height))
    w.focus_set()
    w.bind("<Key>", key)
    render_all_pages(page)
    w.yview_moveto(pointer)

with open('/home/dalanicolai/git/tkpymupdf/help.txt') as f:
    help_text = f.read()

pointer = 0
goto = []
highlight_mode = 0
hstart = []
hend = []
canvas = 1
main_page = 0

def mouse_wheel(event):
    global main_page, pointer
    if event.num == 4:
        print(int(round(point_to_page())), main_page)
        if int(round(point_to_page())) != main_page:
            main_page = int(point_to_page())
            page = int(point_to_page())
            print(page)
            if page == page_count - 1:
                return
            if not page == 0:
                w.delete(image_dict[page - 1])
                try: tkimdict.pop(page - 1)
                except: pass
            pointer += 1 / (10 * page_count)
            page = int(point_to_page())
            if not page == page_count - 1:
                render_page(page + 1, zoom_factor)
        else:
            pointer += 1 / (10 * page_count)
            print(main_page)
        w.yview_moveto(pointer)

def key(event):
    print(event.keysym)
    global pointer, goto, main_page
    global highlight_mode, hstart, hend, canvas
    global image_dict, tkimdict, dlist
    global w, zoom_factor, doc
    global help_text
    if highlight_mode == 1 and not event.keysym == 'Escape':
        hstart.append(event.keysym)
        print(hstart)
        if len(hstart) == 2:
            highlight_mode = 2
    elif highlight_mode == 2 and not event.keysym == 'Escape':
        hend.append(event.keysym)
        if len(hend) == 2:
            #print('helo')
            highlight_mode = 0
            page = int(point_to_page())
            dlist[page] = annotation.create_annot(''.join(hstart), ''.join(hend), doc, page).getDisplayList()
            w.delete(image_dict[page])
            imdata = get_page(page, zoom_factor)
            tkimdict[page] = tk.PhotoImage(data=imdata)
            #print(type(tkimdict[page]))
            create_pageim(page)
            #print('hl =', highlight_mode)
            hstart=[]
            hend=[]
    elif (highlight_mode == 1 or highlight_mode == 2) and  event.keysym == 'Escape':
        highlight_mode = 0
        page = int(point_to_page())
        w.delete(image_dict[page])
        imdata = get_page(page, zoom_factor)
        tkimdict[page] = tk.PhotoImage(data=imdata)
        # print(type(tkimdict[page]))
        create_pageim(page)
        # print('hl =', highlight_mode)
        hstart = []
        hend = []
    elif event.keysym.isnumeric():
        goto.append(event.keysym)
    elif event.keysym == 'G':
        page = int(point_to_page())
        delete_all_pages(page)
        if len(goto) == 0:
            pointer = (page_count-1) * height / doc_height
        else:
            dest = int(''.join(goto))
            pointer = (dest-1) * height / doc_height
        page = int(point_to_page())
        print(page)
        render_all_pages(page)
        w.yview_moveto(pointer)
        goto = []
    elif event.keysym == 'g':
        page = int(point_to_page())
        delete_all_pages(page)
        pointer = 0
        page = int(point_to_page())
        render_all_pages(page)
        w.yview_moveto(pointer)
    elif event.keysym in ['j', 'Down']:
        print(int(round(point_to_page())), main_page)
        if int(round(point_to_page())) != main_page:
            main_page = int(point_to_page())
            page = int(point_to_page())
            print(page)
            if page == page_count - 1:
                return
            if not page == 0:
                w.delete(image_dict[page - 1])
                try: tkimdict.pop(page - 1)
                except: pass
            pointer += 1 / (10 * page_count)
            page = int(point_to_page())
            if not page == page_count - 1:
                render_page(page + 1, zoom_factor)
        else:
            pointer += 1 / (10 * page_count)
            print(main_page)
        w.yview_moveto(pointer)
        #w.delete(im1)
    elif event.keysym in ['k', 'Up']:
        if int(round(point_to_page())) != main_page:
            main_page = int(point_to_page())
            page = int(point_to_page())     # define function, also used in 'K'
            if page == 0:
                return
            if not page == page_count - 1:
                w.delete(image_dict[page + 1])
                try: tkimdict.pop(page + 1)
                except: pass
            pointer -= 1 / (10 * page_count)
            page = int(point_to_page())
            if not page == 0:
                render_page(page - 1, zoom_factor)
        else:
            pointer -= 1/(10 * page_count)
        w.yview_moveto(pointer)
        #im1 = w.create_image(0, 0, anchor='nw', image=tkimdict[1])

    elif event.keysym in ['J', 'Next']:
    # elif event.keysym in ['J', 'Next']:
        print(width, height)
        page = int(point_to_page())
        print(page)
        if page == page_count-1:
            return
        if not page == 0:
            w.delete(image_dict[page-1])
            try: tkimdict.pop(page-1)
            except: pass
        pointer += 1 / (page_count)
        page = int(point_to_page())
        if not page == page_count-1:
            render_page(page+1, zoom_factor)
        w.yview_moveto(pointer)

    elif event.keysym in ['K', 'Prior']:
    # elif event.keysym in ['K', 'Prior']:
        page = int(point_to_page())
        if page == 0:
            return
        if not page == page_count-1:
            w.delete(image_dict[page + 1])
            try: tkimdict.pop(page+1)
            except: pass
        pointer -= 1 / (page_count)
        page = int(point_to_page())
        if not page == 0:
            render_page(page-1, zoom_factor)
        w.yview_moveto(pointer)
    elif event.keysym == 'i':
        page = point_to_page()
        annotation.create_index(dlist[page], page, w, zoom_factor)
    elif event.keysym == 'H':
        highlight_mode = 1
        page = point_to_page()
        # print('pg',page,'hl',highlight_mode)
        # print(type(dlist[page]))
        if not annotation.create_index(dlist[page], page, w, zoom_factor):
            highlight_mode = 0
            # print('hlreset')
    elif event.keysym == 'colon':
        label.pack_forget()
        entry.pack(fill='x')
        entry.focus_set()
    elif event.keysym == 'v':
        entry.pack_forget()
        label.pack(fill='x')
    elif event.keysym == 'W':
        page = int(point_to_page())
        zoom_factor = screen_width/width
        zoom_refresh(page)
    elif event.keysym == 'w':
        page = int(point_to_page())
        zoom_factor = root.winfo_width()/width
        zoom_refresh(page)
    elif event.keysym in ['equal', 'plus']:
        page = int(point_to_page())
        zoom_factor += 0.5
        zoom_refresh(page)
    elif event.keysym in ['underscore', 'minus']:
        page = int(point_to_page())
        if zoom_factor > 0.5:
            zoom_factor -= 0.5
            zoom_refresh(page)
    elif event.keysym == 'p':
        cwidth = cframe.winfo_width()
        print('cwidth=',cwidth)
        root.geometry(str(cwidth)+'x'+str(int(zoom_factor*pageheight)))
    elif event.keysym == 'F1':
        if canvas == 1:
            w.pack_forget()
            help.pack()
            help.tag_config("a", foreground="gray26")
            help.insert(tk.END, help_text, "a")
            help.config(state=tk.DISABLED)
            canvas = 0
        else:
            help.pack_forget()
            w.pack()
            canvas = 1
    elif event.keysym == 'q': root.quit()

    if pointer < 0: pointer = 0
    if pointer > (page_count - 1)/page_count: pointer = (page_count - 1)/page_count

    if pointer == 0: svar.set(1)
    else:
        svar.set(round(pointer * page_count + 1, 1))
    return pointer


cframe = create_cframe(root, pagewidth, screen_height-46)
svar = tk.StringVar()
label = tk.Label(root, textvariable=svar)
svar.set(title + str(pointer/height))
entry = tk.Entry(root)
w = create_canvas(pagewidth, screen_height-56)
help = tk.Text(cframe, width=int(pagewidth), height=int(screen_height-56))
# root.geometry(str(int(screen_width-280))+'x'+str(int(screen_height-66)) + '+200+0')
root.geometry(str(int(pagewidth))+'x'+str(int(screen_height-66)) + '+200+0')
cframe.pack_propagate(0)
cframe.pack()
label.pack(fill='x')
w.pack()

# windowWidth = root.winfo_reqwidth()
# windowHeight = root.winfo_reqheight()
# print("Width", windowWidth, "Height", windowHeight)
#
# # Gets both half the screen width/height and window width/height
# positionRight = int(root.winfo_screenwidth() / 2 - windowWidth / 2)
# positionDown = int(root.winfo_screenheight() / 2 - windowHeight / 2)
#
# # Positions the window in the center of the page.
# root.geometry("+{}+{}".format(positionRight, positionDown))

def create_pageim(i):
    global w
    im = w.create_image(0, i * zoom_factor * pageheight, anchor='nw', image=tkimdict[i])
    return im

def render(zoom_factor):
    cpage = point_to_page()
    print(cpage)
    if cpage == 0:
        cpage = 1
    elif cpage == page_count:
        cpage = page_count - 1
    for i in [cpage - 1, cpage, cpage + 1]:
        # print(height)
        print('cpage', cpage)
        image_dict[i] = create_pageim(i)
        print(image_dict)
        w.create_line(0, i * pageheight, width, i * pageheight, width=1)


tkimdict = load_images(1, zoom_factor)

image_dict = {}

render(zoom_factor)



if __name__ == "__main__":
    tk.mainloop()
