#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 02:30:20 2019

@author: dalanicolai
"""
import os

from tkinter import *
from tkinter import ttk
import fitz



def get_screensize():
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    return screen_width, screen_height

class Canv(Canvas):
    def __init__(self, parent, filepath):
        super().__init__(parent)
        self.doc = fitz.open(filepath)
        self.page_count = len(self.doc)
        self.toc = self.doc.getToC()
        self.dlists, self.page_sizes = self.get_displaylists()
        self.mode_width, self.mode_height = self.get_mode_size()
        self.zoom_factor = round(screen_height/self.mode_height,2) # fit page height to screen height
        self.doc_height = sum([ps[3] for ps in self.page_sizes]) # calculate size canvas to fit all pages (negelecting zoom factor)
        self.config(width=self.zoom_factor * self.mode_width)
        self.config(scrollregion=(0, 0, self.zoom_factor * self.mode_width, self.zoom_factor * self.doc_height))
        self.pointer = 0 # tracks scroll position canvas in range 0-1
        self.main_page = 0 # tracks surrent main page in render function 

        self.tkimdict = {} 
        self.image_dict = {}

        self.goto = []
        self.show_toc = 0
        
        self.char_index = 0
        self.span_index = 0
        self.line_index = 0
        self.block_index = 0


    def get_displaylists(self):
        dlists = []
        page_sizes = []
        for i in range(self.page_count):
            pagedl = self.doc[i].getDisplayList()
            dlists.append(pagedl)
            page_sizes.append(pagedl.rect)
        return dlists, page_sizes

    def get_mode_size(self):
        '''Get most frequent page horizontal/vertical resolution

        :param orientation (int): 2 = horizontal, 3 = vertical
        :return: integer: most frequent horizontal/vertical page resolution
        '''
        frequency= {}
        for i in self.page_sizes:
            if i[2:4] in frequency.keys():
                frequency[i[2:4]] += 1
            else:
                frequency[i[2:4]]=0
        return max(frequency, key=frequency.get)

    def get_page(self, page):
        ''' return zoom imagedata for page '''
        magn_mat = fitz.Matrix(self.zoom_factor, self.zoom_factor)
        pix = self.dlists[page].getPixmap(matrix=magn_mat, alpha=False)
        imdata = pix.getImageData('ppm')
        return imdata
    
    def get_page_rawdict(self, page):
        return self.doc[page].getTextWords()

    def load_images(self, pno):
        '''Create list of 3 pages with previous, current(pno) and next page

        :param pno (int):
        :return: list: List containing 3 tk photoimages
        '''
        imlist = [self.get_page(i) for i in plist]

        tkimdict = {i:PhotoImage(data=imlist[i]) for i in range(len(plist))}
        return tkimdict
    
    def cursor_coords(self, bbox):
        zbbox = [self.zoom_factor * i for i in bbox]
        return( ( zbbox[0], zbbox[1], zbbox[0], zbbox[3] ) )
    
    def cursor_step_forward(self, event):
        print('ci =', self.char_index)
        print('si =', self.span_index)
        print('li =', self.line_index)
        crawdict = self.get_page_rawdict(self.point_to_page())
        cspan = crawdict['blocks'][self.block_index]['lines'][self.line_index]['spans'][self.span_index]
        cline = crawdict['blocks'][self.block_index]['lines'][self.line_index]
        cblock = crawdict['blocks'][self.block_index]
        print('cl =', len(cspan['chars']))
        print('sl =', len(cline['spans']))
        print('ll =', len(cblock['lines']))
        self.delete(self.cursor)
        self.char_index += 1
        if self.char_index < ( len(cspan['chars']) ):
            self.cursor = canvas.create_line( self.cursor_coords( crawdict['blocks'][self.block_index]['lines'][self.line_index]['spans'][self.span_index]['chars'][self.char_index]['bbox'] ) )
        elif self.span_index < ( len(cline['spans']) ):
            if self.span_index + 2 < ( len(cline['spans']) ):
                self.span_index += 2
                self.char_index = 0
            elif self.line_index < ( len(cblock['lines']) - 1 ):
                self.line_index += 1
                self.span_index = 0
                self.char_index = 0
            else:
                self.block_index += 1
                self.line_index = 0
                self.span_index = 0
                self.char_index = 0
            self.cursor = canvas.create_line( self.cursor_coords( crawdict['blocks'][self.block_index]['lines'][self.line_index]['spans'][self.span_index]['chars'][self.char_index]['bbox'] ) )
#        if self.line_index < ( len(cblock['lines']) ):
#            if self.span_index < ( len(cline['spans']) ):
#                if self.char_index < ( len(cspan['chars']) ):
#                    self.char_index += 1
#                    print(self.char_index)
#                elif self.span_index < ( len(cline['spans']) ):
#                    self.span_index += 2
#                    self.char_index = 0
#            elif self.line_index < ( len(cblock['lines']) ):
#                self.line_index += 1
#                self.span_index = 0
#                self.char_index = 0
#        else:
#            self.block_index += 1
#            self.line_index = 0
#            self.span_index = 0
#            self.char_index = 0
#        self.cursor = canvas.create_line( self.cursor_coords( crawdict['blocks'][self.block_index]['lines'][self.line_index]['spans'][self.span_index]['chars'][self.char_index]['bbox'] ) )

    def create_pageim(self, i):
        im = self.create_image(0, i * self.zoom_factor * self.mode_height, anchor='nw', image=self.tkimdict[i])
        #print( self.get_page_rawdict(i)['blocks'][0]['lines'][0]['spans'][0]['chars'][1] )
        print( self.get_page_rawdict(i))
        canvas.create_line(0, i * self.zoom_factor * self.mode_height, self.zoom_factor *self.mode_width, i * self.zoom_factor * self.mode_height, width=1)

        return im

    def point_to_page(self):
        return int(round(self.pointer * self.page_count))

    def render_page(self, page):
        imdata = self.get_page(page)
        tp = self.get_page_rawdict(page)
        self.tkimdict[page] = PhotoImage(data=imdata)
        self.image_dict[page] = self.create_pageim(page)
        #print('hello',self.image_dict)

    def render(self):
        cpage = self.point_to_page()
        if cpage == 0:
            if len(self.doc) == 1:
                plist = [cpage]
            else:
                plist = [cpage, cpage+1]
        elif cpage == page_count:
            plist = [cpage-2, cpage-1]
        else:
            plist =[cpage - 1, cpage, cpage + 1] 
        for i in plist:
            self.render_page(i)
        self.cursor = canvas.create_line( self.cursor_coords( self.get_page_rawdict(cpage)[0][:4]))
#        self.cursor = canvas.create_line( self.cursor_coords( self.get_page_rawdict(cpage)['blocks'][0]['lines'][0]['spans'][0]['chars'][0]['bbox'] ) )
#            w.create_line(0, i * pageheight, width, i * pageheight, width=1)

    def delete_all_pages(self, page):
        self.delete(self.image_dict[page])
        try: self.tkimdict.pop(page)
        except: pass
        if not page == 0:
            self.delete(self.image_dict[page - 1])
            try: self.tkimdict.pop(page - 1)
            except: pass
        if not page == self.page_count - 1:
            self.delete(self.image_dict[page + 1])
            try: self.tkimdict.pop(page + 1)
            except: pass


    def render_all_pages(self, page):
        if not page in [0, self.page_count - 1]:
            pagelist = [page - 1, page, page + 1]
        elif page == 0:
            pagelist = [page, page + 1]
        elif page == self.page_count - 1:
            pagelist = [page - 1, page]
        for i in pagelist:
            self.render_page(i)

    def key_is_p(self, event):
        cwidth = self.winfo_width()
        root.geometry(str(cwidth)+'x'+str(int(1000)))

    def key_is_tab(self, event):
        if self.show_toc == 0:
            canvas.grid_remove()
            toc.grid(column=0,row=0, sticky=(N,S,E,W))
            toc.selection_set('l1:0')
            toc.focus_set()
            toc.focus('l1:0')
            self.show_toc =1
        elif self.show_toc == 1:
            toc.grid_remove()
            canvas.grid(column=0, row=0, sticky=(N,S))
            self.show_toc = 0
            canvas.focus_set()
        

    def check_pointer(self):
        if self.pointer < 0: self.pointer = 0
        if self.pointer > (self.page_count - 1)/self.page_count:
            self.pointer = (self.page_count - 1)/self.page_count


    def smooth_scroll_down(self, event):
        if int(round(self.point_to_page())) != self.main_page:
            self.main_page = int(self.point_to_page())
            page = int(self.point_to_page())
            if page == self.page_count - 1:
                return
            if not (page == 0 or page == 1):
                canvas.delete(self.image_dict[page - 2])
                try: self.tkimdict.pop(page - 2) 
                except: pass
            self.pointer += 1 / (10 * self.page_count)
            self.check_pointer()
            page = int(self.point_to_page())
            if not page == self.page_count - 1:
                self.render_page(page + 1)
        else:
            self.pointer += 1 / (10 * self.page_count)
            self.check_pointer()
        print(self.pointer)
        canvas.yview_moveto(self.pointer)

    def smooth_scroll_up(self, event):
        if int(round(self.point_to_page())) != self.main_page:
            self.main_page = int(self.point_to_page())
            page = int(self.point_to_page())     # define function, also used in 'K'
            if page == 0:
                return
            if not page == self.page_count - 1:
                self.delete(self.image_dict[page + 2])
                try: self.tkimdict.pop(page + 2)
                except: pass
            self.pointer -= 1 / (10 * self.page_count)
            self.check_pointer()
            page = int(self.point_to_page())
            if not page == 0:
                self.render_page(page - 1)
        else:
            self.pointer -= 1/(10 * self.page_count)
            self.check_pointer()
        self.yview_moveto(self.pointer)
    
    def scroll_page_down(self, event):
        page = int(self.point_to_page())
        if page == self.page_count-1:
            return
        if not page == 0:
            self.delete(self.image_dict[page-1])
            try: self.tkimdict.pop(page-1)
            except: pass
        self.pointer += 1 / (self.page_count)
        self.check_pointer()
        page = int(self.point_to_page())
        if not page == self.page_count-1:
            self.render_page(page+1)
        self.yview_moveto(self.pointer)

    def scroll_page_up(self, event):
        page = int(self.point_to_page())
        if page == 0:
            return
        if not page == self.page_count-1:
            self.delete(self.image_dict[page + 1])
            try: self.tkimdict.pop(page+1)
            except: pass
        self.pointer -= 1 / (self.page_count)
        self.check_pointer()
        page = int(self.point_to_page())
        if not page == 0:
            self.render_page(page-1)
        self.yview_moveto(self.pointer)

    def go_to_start(self, event):
        page = int(self.point_to_page())
        self.delete_all_pages(page)
        self.pointer = 0
        page = int(self.point_to_page())
        self.render_all_pages(page)
        self.yview_moveto(self.pointer)


    def key(self, event):
        print(event.keysym)
        if event.keysym.isnumeric():
            self.goto.append(event.keysym)
            print('goto',self.goto)
        elif event.keysym == 'G':
            page = int(self.point_to_page())
            self.delete_all_pages(page)
            if len(self.goto) == 0:
                self.pointer = (self.page_count-1) * self.mode_height / self.doc_height
            else:
                dest = int(''.join(self.goto))
                print(dest)
                self.pointer = (dest-1) * self.mode_height / self.doc_height
                self.check_pointer()
            page = int(self.point_to_page())
            self.render_all_pages(page)
            self.yview_moveto(self.pointer)
            self.goto = []

    def create_toc(self):
        toc = ttk.Treeview(root, columns=('page'), show='tree')
        toc.column('#0', width=100)
        toc.column('page', width=40, anchor='e')
        for i in range(len(self.toc)):
            if self.toc[i][0] == 1:
                toc.insert('', 'end', 'l1:{}'.format(i), text=self.toc[i][1], values=(str(self.toc[i][2])))
                l1 = i
            elif self.toc[i][0] == 2:
                toc.insert('l1:{}'.format(l1), 'end', 'l2:{}'.format(i), text=self.toc[i][1], values=(str(self.toc[i][2])))
                l2 = i
            elif self.toc[i][0] == 3:
                toc.insert('l2:{}'.format(l2), 'end', 'l3:{}'.format(i), text=self.toc[i][1], values=(str(self.toc[i][2])))
                l3 = i
            elif self.toc[i][0] == 4:
                toc.insert('l3:{}'.format(l3), 'end', 'l4:{}'.format(i), text=self.toc[i][1], values=(str(self.toc[i][2])))

#        toc.insert('', 'end', 'ch1', text='Ch1')
#        toc.insert('ch1','end',text='hello')
#        toc.insert('', 'end', 'ch2', text='test')
        return toc

    def toc_select(self, event):
        page = int(self.point_to_page())
        self.delete_all_pages(page)
        item = toc.selection()
        page = toc.item(item,"value")[0]
        dest = int(page)
        self.pointer = (dest-1) * self.mode_height / self.doc_height
        self.check_pointer()
        page = int(self.point_to_page())
        self.render_all_pages(page)
        self.yview_moveto(self.pointer)
        toc.grid_remove()
        canvas.grid(column=0, row=0, sticky=(N,S))
        self.show_toc = 0
        canvas.focus_set()

    def enter_search(self, event):
        entry.grid(column=0, row=1, sticky=(W,E))
        entry.insert(0, '/')
        entry.focus_set()


    def process_entry(self, event):
        query = entry.get()
        if query[0] == '/':             # search
            results = []
            for i in range(len(self.doc)):
                page_results = self.doc[i].searchFor(query[1:])
                if page_results:
                    results.append((i,page_results))
                    print(results)
        
if len(sys.argv) == 2:
    fname = sys.argv[1]
else:
    #fname ='/home/dalanicolai/git/tkpymupdf/test.pdf'
    fname = '/home/dalanicolai/Documents/Algemeen-CV-nederlands.pdf'


root = Tk()
root.configure(bg='black')
#root.config(cursor='sailboat')
screen_width, screen_height = get_screensize()
#cframe = Frame(root, borderwidth=0)
canvas = Canv(root, fname)
canvas.config(highlightthickness=0)
toc = canvas.create_toc()
entry = Entry(root) 
l = Label(root, text='Hello') 
#cframe.grid(sticky=(N,S,W,E))
canvas.grid(column=0, row=0, sticky=(N,S))
#l.grid(column=0, row=1, sticky=(W,E))
root.rowconfigure(0, weight=1)
root.columnconfigure(0, weight=1)
#cframe.rowconfigure(0, weight=1)
root.update_idletasks()
cwidth = canvas.winfo_width()
root.geometry(str(cwidth)+'x'+str(screen_height)+'+0+0')
#l.grid_remove()

canvas.render()
#canvas.create_pageim(0)
#im1 = canvas.create_image(0, 0, anchor='nw', image=canvas.tkimdict[0])
canvas.focus_set()
#canvas.bind('<Key>', lambda e: print(e.keysym))
canvas.bind('p', canvas.key_is_p)
canvas.bind('j', canvas.smooth_scroll_down)
canvas.bind('<Down>', canvas.smooth_scroll_down)
canvas.bind('k', canvas.smooth_scroll_up)
canvas.bind('<Up>', canvas.smooth_scroll_up)
canvas.bind('l', canvas.cursor_step_forward)
canvas.bind('J', canvas.scroll_page_down)
canvas.bind('<Next>', canvas.scroll_page_down)
canvas.bind('K', canvas.scroll_page_up)
canvas.bind('<Prior>', canvas.scroll_page_up)
canvas.bind('g', canvas.go_to_start)
canvas.bind("<Key>", canvas.key)
canvas.bind('d', lambda e: canvas.grid_remove())
canvas.bind('c', lambda e: canvas.grid())
canvas.bind('/', canvas.enter_search)
entry.bind('<Return>', canvas.process_entry)
canvas.bind('s', lambda e: l.grid(column=0, row=1, sticky=(W,E)))
canvas.bind('x', lambda e: l.grid_remove())
canvas.bind('<Tab>', canvas.key_is_tab)
toc.bind('<Tab>', canvas.key_is_tab)
toc.bind('<Return>', canvas.toc_select)
root.bind('q', lambda e: root.quit()) 

root.mainloop()
