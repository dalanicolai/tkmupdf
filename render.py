import sys

from tkinter import PhotoImage
import fitz

if len(sys.argv) == 2:
    fname = sys.argv[1]
else:
    fname ='/home/dalanicolai/Downloads/2002_Book_AngularMomentumTechniquesInQua.pdf'

doc = fitz.open(fname)
page_count = len(doc)
pages = 147

title = "PyMuPDF display of '%s', pages: %i" % (fname, pages)

def get_page(pno):
    dlist = doc[pno].getDisplayList()
    r = dlist.rect
    pix = dlist.getPixmap(alpha=False)
    width = r.width
    height = r.height
#    img = pix.getImageData("ppm")
    img = pix.getImageData("png")
    return img, width, height

def get_imdata(i):
    imdata, width, height = get_page(i)
    return imdata, width, height

def load_images(no):
    pagelist = [no - 1, no, no + 1]
    imlist = [get_imdata(i) for i in pagelist]
    width=imlist[1][1]
    height=imlist[1][2]
    tkimlist = [PhotoImage(data = imlist[i][0]) for i in range(3)]
    data = [tkimlist, width, height, width, height]
    return data
