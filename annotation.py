import fitz


def create_index(page, h, w, zoom):
    tp = page.getTextPage()
    rd = tp.extractRAWDICT()
    print(rd)
    rdb = rd['blocks']
    if not rdb:
        return

    r = page.rect
    pix = page.getPixmap(matrix = fitz.Matrix(zoom, zoom), alpha=False)
    width = zoom * r.width
    height = zoom * r.height
    img = pix.getImageData("ppm")

    capitals = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
    nums = [i for i in range(40)]
    alphabet = list('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    alphas = alphabet
    alphagen = (i for i in alphabet)



# mag=2
# tkimg = tk.PhotoImage(data = img)
#w = tk.Canvas(root, width=mag*width, height=mag*height)
# w = tk.Canvas(root)
# w.focus_set()
# w.bind("<Key>", key)
#w.pack()
# w.pack(fill='x', expand=1)
# w.create_image(0, 0, anchor='nw', image=tkimg)
# w.create_image(mag*width, 0, anchor='nw', image=tkimg)
# e = tk.Entry(root)
# e.pack(fill='x')

#     f = open('check.txt','w+')
#     for k in range(len(rdb[1]['lines'])):
#         for j in range(len(rdb[1]['lines'][k]['spans'])):
#             for i in rdb[1]['lines'][k]['spans'][j]['chars']:
#                 f.write(str(k)+' '+str(j)+'\n'+str(i)+'\n')
#     f.close()

    global index_dict
    index_dict = {}
    for i in range(len(rdb)):
        quad = fitz.Rect(rdb[i]['bbox']).quad
        pos = [tuple(i for i in rdb[i]['bbox'])[x] * zoom + (0, h*height, 0, h*height +1)[x] for x in range(4)]
        index_pos = [pos[y] + (-15 * zoom, 1 * zoom)[y] for y in range(2)]
        # w.create_rectangle(pos)
        # label = capitals[i]
        # w.create_text(*index_pos, text=label)
        # index_dict[label] = quad
        line = rdb[i]['lines']
        for j in range(len(line)):
            quad = fitz.Rect(line[j]['bbox']).quad
            pos = [tuple(i for i in line[j]['bbox'])[x] * zoom + (0, h*height, 0, h*height)[x] for x in range(4)]
            index_pos =[pos[y] + (-12 * zoom, 8 * zoom)[y] for y in range(2)]
            label = next(alphagen)
            #w.create_rectangle(pos,outline='red')
            w.create_text(*index_pos, font=("Purisa", int(zoom * 8)), fill='blue', text=label)
            calphagen = (z for z in alphabet)
            index_dict[label] = quad
            is_word = 0
            word = []
            word_length = 0
            span = line[j]['spans']
            for l in range(len(span)):
                char = span[l]['chars']
                for k in range(len(char)):
                    if char[k]['c'].isalpha() or char[k]['c'].isnumeric() :
                        is_word = 1
                        word.append(fitz.Rect(char[k]['bbox']).quad)
                        word_length += 1
                        if (k + 1) == len(char):
                            quad = fitz.Quad(word[0][0], word[word_length - 1][1], word[0][2], word[word_length - 1][3])
                            pos = ([quad[0][x] * zoom + (0, h * height, 0, h * height)[x] for x in range(2)]
                                   + [quad[3][x] * zoom + (0, h * height, 0, h * height)[x] for x in range(2)])
                            is_word = 0
                            word = []
                            word_length = 0
                            # pos =[tuple(i for i in span[0]['chars'][k]['bbox'])[x] * zoom + (0, h*height, 0, h*height)[x] for x in range(4)]
                            index_pos = [pos[y] + (3 * zoom, 3 * zoom)[y] for y in range(2)]
                            # w.create_rectangle(pos, outline='blue') #, dash=(2,5))
                            # if span[0]['chars'][k-1]['c'] not in alphas and span[0]['chars'][k]['c'] in alphas:
                            clabel = next(calphagen)
                            bb = w.create_text(*index_pos, font=("Purisa", int(zoom * 6)), fill='black', text=clabel)
                            rt = w.create_rectangle(w.bbox(bb), fill="orange", width=0)
                            w.tag_lower(rt, bb)
                            index_dict[label + clabel] = quad
                    elif not char[k]['c'].isalpha() and (char[k-1]['c'].isalpha() or char[k-1]['c'].isnumeric()):
                        # print(span[0]['chars'][k]['c'], span[0]['chars'][k-1]['c'])
                        if not word:
                            continue
                        quad = fitz.Quad(word[0][0], word[word_length - 1][1], word[0][2], word[word_length - 1][3])
                        pos = ([quad[0][x] * zoom + (0, h * height, 0, h * height)[x] for x in range(2)]
                                +[quad[3][x] * zoom + (0, h * height, 0, h * height)[x] for x in range(2)])
                        is_word = 0
                        word = []
                        word_length = 0
                    # pos =[tuple(i for i in span[0]['chars'][k]['bbox'])[x] * zoom + (0, h*height, 0, h*height)[x] for x in range(4)]
                        index_pos=[pos[y] + (3 * zoom, 3 * zoom)[y] for y in range(2)]
                        # w.create_rectangle(pos, outline='blue') #, dash=(2,5))
                        # if span[0]['chars'][k-1]['c'] not in alphas and span[0]['chars'][k]['c'] in alphas:
                        clabel = next(calphagen)
                        bb = w.create_text(*index_pos, font=("Purisa", int(zoom * 6)), fill='blue', text=clabel)
                        rt = w.create_rectangle(w.bbox(bb), fill="orange", width=0)
                        w.tag_lower(rt, bb)
                        index_dict[label+clabel] = quad
    return True

def create_annot(hstart, hend, doc, page):
    global index_dict
    quad1 = index_dict[hstart]
    quad2 = index_dict[hend]
    doc[page].addHighlightAnnot(fitz.Quad(quad1[0],quad2[1],quad1[2],quad2[3]))
    newpage = doc[page]
    return newpage
    # doc.save("new-annots.pdf", expand=255)
