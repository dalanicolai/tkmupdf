#!/usr/bin/env python3

import sys
import tkinter as tk
import fitz

from PIL import Image, ImageDraw, ImageTk

if len(sys.argv) == 2:
    fname = sys.argv[1]
else:
    fname ='/home/dalanicolai/Downloads/2002_Book_AngularMomentumTechniquesInQua.pdf'

root = tk.Tk()
root.title('pyPdfEditor')

doc = fitz.open(fname)
page_count = len(doc)
#pages = 5

title = "PyMuPDF display of '%s', pages: %i" % (fname, page_count)

def get_page(pno):
    dlist = doc[pno].getDisplayList()
    r = dlist.rect
    pix = dlist.getPixmap(alpha=False)
    width = r.width
    height = r.height
    img = pix.getImageData("ppm")
    return img, width, height

def key(event):
    if event.keysym == 'q':
        root.quit()
    if event.keysym == 'j':
        w.yview_scroll(1, 'units')
    if event.keysym == 'k':
        w.yview_scroll(-1, 'units')
    if event.keysym == 'c':
        root.title('yep')

def get_imdata(i):
    imdata, width, height = get_page(i)
    return imdata, width, height

def load_images(no):
#    global pages
    pages = 1
    lst = [7]
    imlist = [get_imdata(i) for i in lst]
    width=imlist[0][1]
    height=imlist[0][2]
    tkimlist = [tk.PhotoImage(data = imlist[i][0]) for i in range(pages)]
    w = tk.Canvas(root, width=width, height=pages*height)
    w.config(scrollregion=(0, 0, width, pages*height))
    w.focus_set()
    w.bind("<Key>", key)
    return tkimlist, w, width, height

def cursor_coords(bbox):
    return( ( bbox[0], bbox[1], bbox[0], bbox[3] ) )

tkimlist, w, width, height = load_images(5)
w.pack()

page = doc.loadPage(7)
tp = page.getText("rawdict")
page_size = ( int(page.rect[2]), int(page.rect[3]) )
img = Image.new('RGBA', page_size, (0,0,0,0))
drw = ImageDraw.Draw(img, 'RGBA')
#drw.rectangle([0,0,lst[2]-lst[0],lst[3]-lst[1]], (255, 0, 0, 96) )
del drw
img2 = ImageTk.PhotoImage(image=img)

for i in range(1):
    w.create_image(0, i*height, anchor='nw', image=tkimlist[i])
    #w.create_image(lst[0], lst[1], anchor='nw', image=img2)
    cursor = w.create_line( cursor_coords( tp['blocks'][1]['bbox'] ) )
    w.delete(cursor)

if __name__ == "__main__":
    tk.mainloop()
