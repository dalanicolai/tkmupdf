from Tkinter import *

import render
import scrollarea   # separate file for canvas class

class App(Frame):
    def __init__(self, parent=None):
        Frame.__init__(self, parent, width=300, height=400)
        self.pack()
        self.cframe = Frame(self)
        #canv_data = render.load_images(4)
        self.canv = scrollarea.ScrollArea(self.cframe)
        self.entry = Entry(self, bd=0, relief='flat')
        self.label = Label(self, bg='white', text="Hello, world!")

        #self.canv.create_image(150, 0, anchor='nw', image=canv_data[0][0])

        self.cframe.pack()
        self.canv.pack()
        self.label.pack(fill='x')


        self.canv.focus_set()
        self.canv.bind('<Key>', self.keypress)  # set event handler


    def keypress(self, event):
        print(event.keysym)
        if event.keysym == 'q':
            root.quit()
        if event.keysym == 'j':
            self.canv.yview_scroll(1, 'units')
        if event.keysym == 'k':
            self.canv.yview_scroll(-1, 'units')
        if event.keysym == 'c':
            root.title('yep')
        if event.keysym == 'colon':
            self.label.pack_forget()
            self.entry.pack(fill='x')
            self.entry.focus_set()
        if event.keysym == 'v':
            self.entry.pack_forget()
            self.label.pack(fill='x')
        if event.keysym.isnumeric():
            self.goto.append(event.keysym)
        if event.keysym == 'Return':
            print(self.goto)
            self.goto = []



root = Tk()

if __name__ == '__main__': App(root).mainloop()
