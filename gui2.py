from tkinter import *

import render

class ScrolledCanvas(Frame):
    def __init__(self, parent=None):
        Frame.__init__(self, parent)
        self.pack(expand=YES, fill=BOTH)
        cframe = Frame(self)
        cframe.pack(expand=YES, fill=BOTH)
        canv = Canvas(cframe)
        canv.config(width=300, height=200)
        canv.config(scrollregion=(0, 0, 300, 400))
        canv.config(highlightthickness=0)
        e = Entry(self)

        # sbar = Scrollbar(cframe)
        # sbar.config(command=canv.yview)
        # canv.config(yscrollcommand=sbar.set)
        # sbar.pack(side=RIGHT, fill=Y)
        canv.pack(side=LEFT, expand=YES, fill=BOTH)
        e.pack(fill='x')

        #canv_data = render.load_images(8)
        #print(type(canv_data[0][0]))
        gif1 = PhotoImage(file='tst.gif')
        canv.create_image(0, 0, image=gif1)

        for i in range(10):
            canv.create_text(150, 50 + (i * 100), text='spam' + str(i), fill='blue')
        canv.focus_set()
        canv.bind('<Key>', self.onDoubleClick)  # set event handler
        self.canvas = canv

    def onDoubleClick(self, event):
        if event.keysym == 'q':
            root.quit()
        if event.keysym == 'j':
            self.canvas.yview_scroll(1, 'units')
        if event.keysym == 'k':
            self.canvas.yview_scroll(-1, 'units')
        if event.keysym == 'c':
            root.title('yep')


root = Tk()

if __name__ == '__main__': ScrolledCanvas(root).mainloop()